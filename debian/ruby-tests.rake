require 'gem2deb/rake/spectask'

require 'fileutils'
require 'tmpdir'

tmpdir = Dir.mktmpdir
at_exit { FileUtils.rm_rf(tmpdir) }
FileUtils.cp('Gemfile', tmpdir)
FileUtils.cp('arbre.gemspec', tmpdir)
ENV['BUNDLE_GEMFILE'] = tmpdir + '/Gemfile'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end
